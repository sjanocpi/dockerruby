# ruby docker

## resources
 https://www.ruby-lang.org/en/downloads/

 https://rmagick.github.io/usage.html

## code pour install
`docker build -t ruby_rmagick .`

`docker run -d  -it   --name ruby_rmagick_cont   -v "$(pwd)"/app:/carto   ruby_rmagick`

`docker exec -it ruby_rmagick_cont bash`

` gem install rmagick`

## code pour générer une image

` ruby image2.rb`

## code pour lancement

`docker start ruby_rmagick_cont`

`docker exec -it ruby_rmagick_cont bash`


